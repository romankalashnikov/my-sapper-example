import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


import saperBomb.Box;
import saperBomb.Coord;
import saperBomb.Field;
import saperBomb.Game;

public class Sapper extends JFrame {

    private Game game;
    private JLabel lable;
    private JPanel panel;
    private final int easyCols = 9;
    private final int normalCols = 16;
    private final int hardCols = 16;

    private final int easyRows = 9;
    private final int normalRows = 16;
    private final int hardRows = 20;

    private final int easyBomb = 10;
    private final int normalBomb = 40;
    private final int hardBomb = 60;

    private final int sizeImage = 50;
    private JButton easy;
    private JButton normal;
    private JButton hard;


    public static void main(String[] args) {
        new Sapper();
    }

    private Sapper(){
        addButtonAndChoiceOfDifficulty();
    }

    private void initLable(){
        lable = new JLabel("Welcome To Sapper");
        add(lable,BorderLayout.NORTH);
    }
    private void initPanel(){
        panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                for(Coord coord : Field.getAllCords()){

                    g.drawImage((Image) game.getBomb(coord).image, coord.x * sizeImage  , coord.y * sizeImage, this);

                }
            }
        };

        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                int x = e.getX()/sizeImage;
                int y = e.getY()/sizeImage;
                Coord coord = new Coord(x,y);

                if(e.getButton() == MouseEvent.BUTTON1){
                    game.pressLeftButton(coord);
                }
                if(e.getButton() == MouseEvent.BUTTON3){
                    game.pressRightButton(coord);
                } if(e.getButton() == MouseEvent.BUTTON2){
                    game.start();
                }
                lable.setText(getMessage());
                panel.repaint();
            }
        });
        panel.setPreferredSize(new Dimension(
                Field.getSize().x * sizeImage,
                Field.getSize().y * sizeImage));
        add(panel);
    }

    private String getMessage() {
        switch (game.getState()){
            case PLAYED:return "Good Luck ;)";
            case BOMBED:return "Next time...";
            case WINNER:return "Winner Winner Winner ";
            default: return "Welcome To Sapper";
        }
    }

    public void addButtonAndChoiceOfDifficulty(){
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));


        /**
         * Добавляем кнопки и именуем их
         */
        buttonsPanel.add(javax.swing.Box.createRigidArea(new Dimension(0, 50)));
        easy = new JButton("Easy - 9x9, 10 mine");
        buttonsPanel.add(easy);
        easy.setAlignmentX(Component.CENTER_ALIGNMENT);
        normal = new JButton("Normal - 16x16, 40 mine");
        buttonsPanel.add(javax.swing.Box.createRigidArea(new Dimension(0, 30)));

        buttonsPanel.add(normal);
        normal.setAlignmentX(Component.CENTER_ALIGNMENT);
        hard = new JButton("Hard - 16x20, 60 mine");
        buttonsPanel.add(javax.swing.Box.createRigidArea(new Dimension(0, 30)));

        buttonsPanel.add(hard);
        hard.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(buttonsPanel);
        buttonsPanel.add(javax.swing.Box.createRigidArea(new Dimension(0, 30)));

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sapper");
        setSize(300, 300);
        setVisible(true);

        setLocationRelativeTo(null);


        easy.addActionListener(e -> {
            buttonsPanel.setVisible(false);
            game = new Game(easyCols, easyRows, easyBomb);
            game.start();
            initPanel();
            unitFrame();
            initLable();
            setImages();
            System.out.println("21312");
        });

        normal.addActionListener(e -> {
            buttonsPanel.setVisible(false);
            game = new Game(normalCols, normalRows, normalBomb);
            game.start();
            initPanel();
            unitFrame();
            initLable();
            setImages();
            System.out.println("21312");

        });

        hard.addActionListener(e -> {
            buttonsPanel.setVisible(false);
            game = new Game(hardCols, hardRows, hardBomb);
            game.start();
            initPanel();
            unitFrame();
            initLable();
            setImages();
            System.out.println("21312");

        });

        }

    private void unitFrame(){

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sapper");
        setResizable(false);
        setVisible(true);
        pack();
        setLocationRelativeTo(null);

    }
    private void setImages(){
        for(Box box : Box.values()){
            box.image = getImage(box.name());
        }

    }
    private Image getImage(String name){
        String filename = "img/" + name.toLowerCase() + ".png";
        ImageIcon icon = new ImageIcon(getClass().getResource(filename));
        return icon.getImage();
    }

}
