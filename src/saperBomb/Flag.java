package saperBomb;

public class Flag {
    private Matrix flagMap;
    private int countOfClosedBoxes;

    void start() {
        flagMap = new Matrix(Box.CLOSED);
        countOfClosedBoxes = Field.getSize().x * Field.getSize().y;

    }

    Box get(Coord coord) {
        return flagMap.get(coord);
    }

    public void setOpenedToBox(Coord coord) {
        flagMap.set(coord, Box.OPENED);
        countOfClosedBoxes--;

    }




    public void toogleFlagedToBox(Coord coord) {
        switch (flagMap.get(coord)) {
            case FLAGED:
                setClosedToBox(coord);
                break;
            case CLOSED:
                setFlagedToBox(coord);
                break;

        }
    }


    public void setFlagedToBox(Coord coord) {
            flagMap.set(coord, Box.FLAGED);
    }

    private void setClosedToBox(Coord coord) {
        flagMap.set(coord, Box.CLOSED);
    }


    int getCountOfClosesBoxes() {
        return countOfClosedBoxes;
    }

    void setBombedToBox(Coord coord) {
        flagMap.set(coord, Box.BOMBED);
    }

    public void setOpenedToClosedBombBox(Coord coord) {
        if(flagMap.get(coord) == Box.CLOSED){
            flagMap.set(coord,Box.OPENED);
        }
    }


    public void setNoBombsToFlagedBox(Coord coord) {
        if(flagMap.get(coord) == Box.FLAGED){
            flagMap.set(coord,Box.NOBOMB);
        }
    }


    int getCountOfFlagedBoxesAround(Coord coord) {
        int count = 0;
        for(Coord around: Field.getCoordsAround(coord)){
            if(flagMap.get(around) == Box.FLAGED){
                count++;
            }
        }
        return count;
    }
}
