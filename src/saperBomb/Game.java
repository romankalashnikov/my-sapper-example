package saperBomb;

public class Game {
    private Bomb bomb;
    private Flag flag;
    private GameState state;

    public GameState getState() {
        return state;
    }


    public Game(int cols, int rows, int bombs){
        Field.setSize(new Coord(cols,rows));
        bomb = new Bomb(bombs);
        flag = new Flag();
    }
    public void start(){
        bomb.start();
        flag.start();
        state = GameState.PLAYED;
    }
    public Box getBomb(Coord coord){
        if(flag.get(coord)==Box.OPENED)
            return bomb.get(coord);
        else
        return flag.get(coord);

    }

    public void pressLeftButton(Coord coord) {
        if(gameOver()) return;
        openBox(coord);
        checkWinner();

    }
    private void checkWinner(){
        if(state == GameState.PLAYED){
            if(flag.getCountOfClosesBoxes() == bomb.getTotalBombs()){
                state = GameState.WINNER;
            }
        }
    }
    private void openBox(Coord coord){
        switch (flag.get(coord)){
            case OPENED:setOpenedToClosesBoxesAroundNumber(coord); return;
            case FLAGED:return;
            case CLOSED:
                switch (bomb.get(coord)){
                    case ZERO:openBoxAround(coord);return;
                    case BOMB:openBombs(coord);return;
                    default: flag.setOpenedToBox(coord); return;
                }
        }
    }
    void setOpenedToClosesBoxesAroundNumber(Coord coord){
        if(bomb.get(coord) != Box.BOMB){
            if(flag.getCountOfFlagedBoxesAround(coord) == bomb.get(coord).getNumber()){
                for(Coord around : Field.getCoordsAround(coord)){
                    if(flag.get(around) == Box.CLOSED){
                        openBox(around);
                    }
                }
            }
        }

    }

    private void openBombs(Coord bombed) {
        state = GameState.BOMBED;
        flag.setBombedToBox(bombed);
        for(Coord coord: Field.getAllCords()){
            if(bomb.get(coord)==Box.BOMB){
                flag.setOpenedToClosedBombBox(coord);
            }
            else flag.setNoBombsToFlagedBox(coord);
        }
    }

    private void openBoxAround(Coord coord) {
        flag.setOpenedToBox(coord);
        for (Coord around : Field.getCoordsAround(coord)){
            openBox(around);
        }
    }

    public void pressRightButton(Coord coord) {
        if(gameOver()) return;
        flag.toogleFlagedToBox(coord);
    }
    private boolean gameOver(){
        if(state == GameState.PLAYED){
            return false;
        }else start();
            return true;
    }

}
