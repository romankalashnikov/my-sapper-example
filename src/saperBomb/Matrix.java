package saperBomb;

class Matrix {
     private Box[][] matrix;
     Matrix(Box defaultBox){
         matrix = new Box[Field.getSize().x ][Field.getSize().y];
         for(Coord coord: Field.getAllCords()){
             matrix[coord.x ][coord.y] = defaultBox;
         }

     }
     Box get(Coord coord){
         if(Field.inRange(coord))
             return matrix[coord.x][coord.y];
         return null;
     }
     void set(Coord coord, Box box){
         if(Field.inRange(coord))
             matrix[coord.x][coord.y] = box;

     }
}
